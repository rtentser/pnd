#!/usr/bin/python3
from os import makedirs
from requests import get
from sys import argv
from urllib.parse import urlsplit, urlunsplit


def parseURL(url):
    splittedURL = urlsplit(url)
    path = splittedURL.path
    return {"url": urlunsplit(splittedURL), "name": path.split('/')[-1]}


def parseNXM(nxm):
    splittedNXM = urlsplit(nxm)
    splittedPath = splittedNXM.path.split('/')
    result = dict(item.split('=') for item in splittedNXM.query.split('&'))
    result["gameID"] = splittedNXM.netloc.lower()
    result["modID"] = splittedPath[2]
    result["fileID"] = splittedPath[4]
    return result


apiKey = argv[1]
nxmLink = argv[2]

headers = {'apikey': apiKey}
nxm = parseNXM(nxmLink)
parameters = {'key': nxm["key"], 'expires': nxm["expires"]}
domain = "https://api.nexusmods.com/"
url = "v1/games/" + nxm["gameID"] + "/mods/" + nxm["modID"] + \
    "/files/" + nxm["fileID"] + "/download_link.json"

r = get(domain + url, headers=headers, params=parameters)
parsedURL = parseURL(r.json()[0].get('URI'))

fileURL = parsedURL["url"]
name = parsedURL["name"]
mod = get(fileURL, headers=headers)

path = "/home/rtentser/.cache/portmod/downloads/"
makedirs(path, exist_ok=True)
f = open(path + name, "bw+")
f.write(mod.content)
f.close()
